// SPDX-License-Identifier: Apache-2.0

/** Documentation for package progmem
  *
  * Write any relevant package-wide documentation here.
  */
package object progmem {}
