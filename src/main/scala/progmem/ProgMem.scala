// SPDX-License-Identifier: Apache-2.0
package progmem

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import scala.math
import a_core_common._
import amba.axi4l._

class ReadArbiterPort(addr_width: Int, data_width: Int) extends Bundle {
    val req = Input(Bool())
    val gnt = Output(Bool())
    val raddr = Input(UInt(addr_width.W))
    val rdata = Output(UInt(data_width.W))
    val rdata_valid = Output(Bool())
}

/** Read port arbiter with priority given to channel 0 */
class ReadArbiter(addr_width: Int, data_width: Int) extends Module {
  val ch0 = IO(new ReadArbiterPort(addr_width, data_width))
  val ch1 = IO(new ReadArbiterPort(addr_width, data_width))
  val read_port = IO(new Bundle {
    val ren   = Output(Bool())
    val raddr = Output(UInt(addr_width.W))
    val gnt_ch = Output(Bool())
    val rdata = Input(UInt(data_width.W))
  })

  val ch0_rdata_valid = RegNext(ch0.gnt)
  val ch1_rdata_valid = RegNext(ch1.gnt)

  val raddr_mux = Mux(ch0.req, ch0.raddr, ch1.raddr)

  ch0.gnt := ch0.req
  ch0.rdata_valid := ch0_rdata_valid
  ch0.rdata := Mux(ch0_rdata_valid, read_port.rdata, DontCare)

  ch1.gnt := !ch0.req && ch1.req
  ch1.rdata_valid := ch1_rdata_valid
  ch1.rdata := Mux(ch0_rdata_valid, DontCare, read_port.rdata)

  read_port.ren := ch0.req || ch1.req
  read_port.gnt_ch := ch1.gnt
  read_port.raddr := raddr_mux
}

/** IO definitions for ProgMem */
class ProgMemIO(depth_bits: Int, addr_width: Int, data_width: Int)
  extends Bundle {
  val programming_iface = new ProgIface(depth_bits=depth_bits) 
  val prog_write_en = Input(Bool())
  val mem_req = Input(Bool())
  val mem_gnt = Output(Bool())
  val mem_raddr = Input(UInt(addr_width.W))
  val mem_rdata  = Output(UInt(data_width.W))
  val mem_rdata_valid = Output(Bool())
  val fault = Output(Bool())
}

/** Chisel module ProgMem
  * @param first_addr First address in RISC-V address space expressed as Chisel hex string.
  * @param depth_bits Width of the memory array address line.
  * @param addr_width AXI4-Lite address width.
  * @param data_width AXI4-Lite data width.
  */
class ProgMem(val first_addr: String, val depth_bits: Int, addr_width: Int, data_width: Int) extends Module with AXI4LSlave {
  // programmer write port, instruction fetch read port
  val io = IO(new ProgMemIO(depth_bits=depth_bits, addr_width=addr_width, data_width=data_width))

  // AXI4-Lite read port
  val clk_rst = IO(new AXI4LClkRst)
  val slave_port = IO(new AXI4LIO(addr_width, data_width))

  // AXI4-Lite to SRAM adapter
  val axi_to_sram = Module(new AXI4LtoSRAM(addr_width, data_width))
  axi_to_sram.clk_rst <> clk_rst
  axi_to_sram.slave_port <> slave_port

  val read_arb = Module(new ReadArbiter(addr_width, data_width))

  // AXI4-Lite read port
  read_arb.ch0.req := axi_to_sram.io.read.req
  read_arb.ch0.raddr := axi_to_sram.io.read.addr
  axi_to_sram.io.read.data.bits := read_arb.ch0.rdata
  axi_to_sram.io.read.gnt := read_arb.ch0.gnt
  axi_to_sram.io.read.data.valid := read_arb.ch0.rdata_valid
  axi_to_sram.io.read.fault := false.B
  axi_to_sram.io.write.gnt := false.B
  axi_to_sram.io.write.fault := false.B

  // Instruction fetch port
  read_arb.ch1.req := io.mem_req
  read_arb.ch1.raddr := io.mem_raddr - first_addr.U  // convert to local address
  io.mem_gnt := read_arb.ch1.gnt
  io.mem_rdata := read_arb.ch1.rdata
  io.mem_rdata_valid := read_arb.ch1.rdata_valid

  // Save fault in a register, so it triggers on the next clock cycle, and not combinatorially
  val fault = WireDefault(false.B)
  io.fault := RegNext(fault, false.B)

  val max_address = first_addr.U.litValue + scala.math.pow(2,depth_bits).toInt

  when (io.mem_raddr < first_addr.U) {
    // Progmem address out of range
    fault := true.B
  } .elsewhen (io.mem_raddr >= max_address.U){
    // Progmem address out of range
    fault := true.B
  } .otherwise {
    fault := false.B
  }
  
  val mem_array = Seq.fill(4)(
    SyncReadMem(scala.math.pow(2,depth_bits-2).intValue, UInt(8.W)) 
  ) 

  // Split the address lines
  // write address (programmer)
  val waddr_lsb = Wire(UInt(2.W))
  val waddr_msb = Wire(UInt((depth_bits-2).W))
  waddr_lsb := io.programming_iface.addr(1,0)
  waddr_msb := io.programming_iface.addr(depth_bits-1,2)

  // Choose where to write
  when (io.prog_write_en) {
    when ( waddr_lsb === 0.U ) {
      mem_array(0).write(waddr_msb, io.programming_iface.data)
    }. elsewhen ( waddr_lsb === 1.U ) {
      mem_array(1).write(waddr_msb, io.programming_iface.data)
    }. elsewhen ( waddr_lsb === 2.U ) {
      mem_array(2).write(waddr_msb, io.programming_iface.data)
    }. elsewhen ( waddr_lsb === 3.U ) {
      mem_array(3).write(waddr_msb, io.programming_iface.data)
    } 
  }

  // read address (read arbiter)
  val raddr_lsb = Wire(UInt(2.W))
  val raddr_msb = Wire(UInt((depth_bits-2).W))
  val isMisaligned = Wire(Bool())
  raddr_lsb := read_arb.read_port.raddr(1,0)
  raddr_msb := read_arb.read_port.raddr(depth_bits-1,2)
  isMisaligned := raddr_lsb === "b10".U && read_arb.read_port.gnt_ch

  val isMisaligned_delayed = RegNext(isMisaligned)

  read_arb.read_port.rdata := DontCare
  when (read_arb.read_port.ren) {
    val mem_a0 = Wire(UInt(8.W))
    val mem_a1 = Wire(UInt(8.W))
    val mem_a2 = Wire(UInt(8.W))
    val mem_a3 = Wire(UInt(8.W))

    val raddr_a10 = Wire(UInt((depth_bits-2).W))

    raddr_a10 := Mux(isMisaligned, raddr_msb + 1.U, raddr_msb)

    mem_a3 := mem_array(3).read(raddr_msb,true.B)
    mem_a2 := mem_array(2).read(raddr_msb,true.B)
    mem_a1 := mem_array(1).read(raddr_a10,true.B)
    mem_a0 := mem_array(0).read(raddr_a10,true.B)      
    
    when(isMisaligned_delayed) {
      read_arb.read_port.rdata := Cat(mem_a1, mem_a0, mem_a3, mem_a2)
    }. otherwise {
      read_arb.read_port.rdata := Cat(mem_a3, mem_a2, mem_a1, mem_a0)
    } 
  }
}

/** Generates verilog */
object ProgMem extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new ProgMem(
    first_addr = "h0000_1000",
    depth_bits = 16,
    addr_width = 32,
    data_width = 32
  )))
  (new ChiselStage).execute(args, annos)
}

